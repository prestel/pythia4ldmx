Welcome to the Jupyter notebooks for Pythia 8!

You have the choice to run the following notebooks:

pythiaEvent.ipynb
  Pythia 8 graphical event visualization, using a default Pythia
  executable. The process can be choosen, as well as a few basic parameters.

pythiaPI.ipynb
  Gives a basic idea of the Pythia 8 event generator, by using the Python 
  interface of Pythia 8. You can adjust a set of parameters and choose 
  from different different histograms to be plotted.

pythiaRivet.ipynb
  Shows how to use Pythia 8, together with Rivet, by using an already 
  compiled executable called main300. You can adjust a set of parameters
  and a settings file is created.

Run the docker container with
   docker run --rm -u `id -u $USER` -v $PWD:$PWD -w $PWD -p 8888:8888 containername
to mount your current working directory $PWD to a container directory with the
same name. This allows the container to access your local files and to write
files to the current working directory.

This allows the following functionality:
- any custom Rivet analysis *.cc found in $PWD is automatically detected and a 
  Rivet library is build
- custom user settings file can be stored here and used in the notebooks
  pythiaRivetUS.ipynb and direRivetUS.ipynb
- the notebooks and all output such as plots or command files are saved on 
  your system automatically
