#!/bin/bash

mv extra/MLL.cc .
mv extra/MLL.plot .


docker build -t kaw-pythia-tutorial .

mv MLL.cc extra/
mv MLL.plot extra/

#docker run --rm -u `id -u $USER` -v $PWD:$PWD -w $PWD -p 8888:8888 kaw-pythia-tutorial

#docker login --username stefanprestel
#docker tag kaw-pythia-tutorial stefanprestel/kaw-pythia-tutorial:1.0.0
#docker tag kaw-pythia-tutorial stefanprestel/kaw-pythia-tutorial:latest
#docker push stefanprestel/kaw-pythia-tutorial
#docker pull stefanprestel/kaw-pythia-tutorial

