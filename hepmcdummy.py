import os

def snn(input_number):
    return str('%.6e' % input_number)

def get_start_hepmc_info():
    return "\n" + "HepMC::Version 2.06.09" + "\n" + \
           "HepMC::IO_GenEvent-START_EVENT_LISTING" + "\n"

def get_end_hepmc_info():
    return "HepMC::IO_GenEvent-END_EVENT_LISTING" + "\n"

def get_event_hepmc_info(iEvent, nEvent, pythia):
    allInfo  = ''
    if iEvent == 0:
        allInfo += get_start_hepmc_info()
    allInfo += "E " + str(iEvent) + \
               " -1 -1.000000e+00 -1.000000e+00 -1.000000e+00" + \
               " 0 0 1 1 2 0 1 1.000000e+00" + "\n"
    event    = pythia.event
    # Event information.
    allInfo += "N 1 \"0\"" + "\n"
    allInfo += "U GEV MM" + "\n"
    # Cross section information.
    convert  = 1000000000.0
    allInfo += "C " + snn(pythia.info.sigmaGen()*convert) + " " + \
               snn(pythia.info.sigmaErr()*convert) + "\n"
    # PDF information.
    allInfo += "F " + str(pythia.info.id1()) + " " + \
               str(pythia.info.id2()) + " " + snn(pythia.info.x1()) + \
               " " + snn(pythia.info.x2()) + " " + \
               snn(pythia.info.QFac()) + " " + snn(pythia.info.pdf1()) + \
               " " + snn(pythia.info.pdf2()) + " 0 0" + "\n"
    # Number of intial- and final-state particles.
    nISpart  = 2
    nFSpart  = 0
    for prt in event:
        if prt.isFinal():
            nFSpart += 1
    allInfo += "V -1 0 0 0 0 0 " + str(nISpart) + " " + \
               str(nFSpart) + " 0" + "\n"
    # Initial-state particles.
    in1, in2 = event[1], event[2]
    allInfo += "P 1 " + str(in1.id()) + " " + snn(in1.px()) + " " + \
               snn(in1.py()) + " " + snn(in1.pz()) + " " + \
               snn(in1.e()) + " " + snn(in1.m()) + " 4 0 0 -1 0" + "\n"
    allInfo += "P 2 " + str(in2.id()) + " " + snn(in2.px()) + " " + \
               snn(in2.py()) + " " + snn(in2.pz()) + " " + \
               snn(in2.e()) + " " + snn(in2.m()) + " 4 0 0 -1 0" + "\n"
    # Final-state particles.
    for i in range(event.size()):
        if event[i].isFinal():
            allInfo += "P " + str(i) + " " + str(event[i].id()) + " " + \
                       snn(event[i].px()) + " " + snn(event[i].py()) + \
                       " " + snn(event[i].pz()) + " " + \
                       snn(event[i].e()) + " " + snn(event[i].m()) + \
                       " 1 0 0 0 0" + "\n"
    if iEvent == nEvent-1:
        allInfo += get_end_hepmc_info()
    return allInfo

def event_hepmc_file(filename, allInfos, first):
    openmode  = "a"
    if first: openmode = "w+"
    writefile = open(filename, openmode)
    writefile.write(allInfos)
    writefile.close()

def event_hepmc_fifo(filename, allInfos, last):
    fifopipe = os.open(filename, os.O_WRONLY)
    os.write(fifopipe, allInfos)
    if last: os.close(fifopipe)

def event_hepmc(filename, iEvent, nEvent, pythia, fifo):
    allInfos = get_event_hepmc_info(iEvent, nEvent, pythia)
    if fifo: event_hepmc_fifo(filename, allInfos, iEvent==nEvent-1)
    else: event_hepmc_file(filename, allInfos, iEvent==0)
