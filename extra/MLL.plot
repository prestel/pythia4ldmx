# BEGIN PLOT /MLL/*.
Title=8 GeV $e$-beam on $p\in T$; DIS cuts: $Q>2$~GeV, $W>\sqrt{2}$~GeV
RatioPlot=0
# END PLOT

# BEGIN PLOT /MLL/mll
YLabel=$d\sigma/dm_{\ell\ell}~~~~~~~~~~~~~~~~~~~~$
XLabel=Invariant mass $m_{\ell\ell}$ of 2nd and 3rd leading lepton (in $E$) [GeV]~~~
# END PLOT

# BEGIN PLOT /MLL/nlepton
YLabel=$d\sigma/dn_{\ell}~~~~~~~~~~~~~~~~~~~~$
XLabel=Inclusive lepton multiplicity 
# END PLOT

# BEGIN PLOT /MLL/nzprime
YLabel=$d\sigma/dn_{A^\prime}~~~~~~~~~~~~~~~~~~~~$
XLabel=Inclusive $A^\prime$ multiplicity 
LegendXPos=0.47
LogY=0
# END PLOT

# BEGIN PLOT /MLL/nphoton
YLabel=$d\sigma/dn_{\gamma}~~~~~~~~~~~~~~~~~~~~$
XLabel=Inclusive $\gamma$ multiplicity 
LegendXPos=0.47
# END PLOT

# BEGIN PLOT /MLL/elepton*
YLabel=$1/\sigma d\sigma/dE_{\ell}~~~~~~~~~~~~~~~~~~~~$
XLabel=Energy of the leading lepton [GeV]
XMax=7
LegendXPos=0.2
LegendYPos=0.4
Rebin=2
# END PLOT

# BEGIN PLOT /MLL/elepton_photonevt*
YLabel=$1/\sigma d\sigma/dE_{\ell}~~~~~~~~~~~~~~~~~~~~$
XLabel=Energy of the leading lepton ($\gamma$ events) [GeV]
XMax=7
LegendXPos=0.2
LegendYPos=0.4
Rebin=2
# END PLOT

# BEGIN PLOT /MLL/elepton_zprimeevt*
YLabel=$1/\sigma d\sigma/dE_{\ell}~~~~~~~~~~~~~~~~~~~~$
XLabel=Energy of the leading lepton ($A^\prime$ events) [GeV]
XMax=7
LegendXPos=0.2
LegendYPos=0.4
Rebin=2
# END PLOT

# BEGIN PLOT /MLL/ephoton*
YLabel=$1/\sigma d\sigma/dE_{\gamma}~~~~~~~~~~~~~~~~~~~~$
XLabel=Energy of the leading photon [GeV]
XMax=7
LegendXPos=0.2
LegendYPos=0.4
Rebin=2
# END PLOT

# BEGIN PLOT /MLL/elepton_photonevt*


# BEGIN PLOT /MLL/ezprime*
YLabel=$d\sigma/dE_{A^\prime}~~~~~~~~~~~~~~~~~~~~$
XLabel=Energy of the leading $A^\prime$ [GeV]
LegendXPos=0.47
# END PLOT

# BEGIN PLOT /MLL/ptzprime*
YLabel=$d\sigma/d$log10$(p_{\perp A^\prime})~~~~~~~~~~~~~~~~~~~~$
XLabel=log10(Transverse momentum of the leading $A^\prime$) [log10(GeV)]
Rebin=4
LegendXPos=0.3
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /MLL/ptphoton*
YLabel=$d\sigma/d$log10$(p_{\perp \gamma})~~~~~~~~~~~~~~~~~~~~$
XLabel=log10($p_\perp$ of the leading photon) [log10(GeV)]
Rebin=2
LegendXPos=0.47
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /MLL/ptlepton*
YLabel=$d\sigma/d$log10$(p_{\perp \ell})~~~~~~~~~~~~~~~~~~~~$
XLabel=log10($p_\perp$ of the leading lepton) [log10(GeV)]
Rebin=2
XMax=1.5
LegendXPos=0.47
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /MLL/ptlepton_zprimeevt*
YLabel=$d\sigma/d$log10$(p_{\perp \ell})~~~~~~~~~~~~~~~~~~~~$
XLabel=log10($p_\perp$ of the leading lepton ($A^\prime$ events)) [log10(GeV)]
Rebin=2
XMax=1.5
LegendXPos=0.47
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /MLL/ptlepton_photonevt*
YLabel=$d\sigma/d$log10$(p_{\perp \ell})~~~~~~~~~~~~~~~~~~~~$
XLabel=log10(Transverse momentum of the leading lepton ($\gamma$ events)) [log10(GeV)]
Rebin=2
XMax=1.5
LegendXPos=0.47
LegendYPos=0.4
# END PLOT

