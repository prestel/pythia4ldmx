// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include <iostream>

namespace Rivet {
    
  /// @brief Add a short analysis description here
  class MLL : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(MLL);

    /// Book histograms and initialise projections before the run
    void init() {

      //const DISKinematics &dk = addProjection(DISKinematics(), "Kinematics");
      //addProjection(DISFinalState(dk, DISFinalState::BREIT), "DISFS");

      IdentifiedFinalState electrons;
      electrons.acceptId(11);
      electrons.acceptId(-11);
      declare(electrons, "electrons");
      IdentifiedFinalState muons;
      muons.acceptId(13);
      muons.acceptId(-13);
      declare(muons, "muons");
      const Cut mincut = Cuts::E > 0.1*GeV;
      IdentifiedFinalState zp(mincut); zp.acceptId(1023); declare(zp, "zprime");
      IdentifiedFinalState ph(mincut); ph.acceptId(22);   declare(ph, "photon");
book(_h["mll"], "mll", 1000,0.,1.0);
book(_h["nlepton"], "nlepton", 6, -0.5, 5.5);  
book(_h["nzprime"], "nzprime", 6, -0.5, 5.5);  
book(_h["nphoton"], "nphoton", 6, -0.5, 5.5);  
book(_h["elepton"], "elepton", 100, 0., 25.);  
book(_h["ezprime"], "ezprime", 100, 0., 25.);  
book(_h["ephoton"], "ephoton", 100, 0., 25.);
book(_h["ptlepton"], "ptlepton", 100, 0., 5.);  
book(_h["ptzprime"], "ptzprime", 100, -4., 1.);  
book(_h["ptphoton"], "ptphoton", 100, -4., 1.);
book(_h["elepton_photonevt"], "elepton_photonevt", 100, 0., 25.);  
book(_h["ptlepton_photonevt"], "ptlepton_photonevt", 100, 0., 5.);  
book(_h["elepton_zprimeevt"], "elepton_zprimeevt", 100, 0., 25.);  
book(_h["ptlepton_zprimeevt"], "ptlepton_zprimeevt", 100, 0., 5.);  

    }

    /// Perform the per-event analysis
    void analyze(const Event& event) {

/*int beamConfig = abs(beamIds().first*beamIds().second);

if (beamConfig == 11*11) {;
} else if (beamConfig == 2212*2212) {;
} else if (beamConfig == 11*2212) {
} 

   const DISFinalState &fs = applyProjection<DISFinalState>(event, "DISFS");
*/
//cout << weight << " " << w << endl;

//cout << maxwt << endl;
//if (abs(weight)>1e-8) vetoEvent;
//if (abs(weight)>5e-9) vetoEvent;
//if (abs(weight)>2e-9) vetoEvent;

       const Particles& muons = apply<FinalState>(event,"muons").particles();
       const Particles& electrons = apply<FinalState>(event,"electrons").particles();
       const Particles& zprime = apply<FinalState>(event,"zprime").particles();
       const Particles& photon = apply<FinalState>(event,"photon").particles();
       sortByE(electrons);
       sortByE(muons);
       sortByE(zprime);
       sortByE(photon);

       vector<FourMomentum> pmupairs;
       vector<FourMomentum> pphoton;
       vector<FourMomentum> pzprime;
       if (muons.size()>=2) {

        FourMomentum pl = muons[muons.size()-1].momentum()
                        + muons[muons.size()-2].momentum();

        double m = (muons[muons.size()-1].momentum()
                   +muons[muons.size()-2].momentum()).mass();

        _h["mll"]->fill( m);

        for (int i=0; i < muons.size(); ++i)
          if (i+1<muons.size()) pmupairs.push_back(
            muons[i].momentum()+muons[i+1].momentum());
        pphoton.insert(pphoton.end(),pmupairs.begin(),pmupairs.end());
        pzprime.insert(pzprime.end(),pmupairs.begin(),pmupairs.end());
       }

       for (int i=0; i < zprime.size(); ++i)
         pzprime.push_back(zprime[i].momentum());
       for (int i=0; i < photon.size(); ++i)
         pphoton.push_back(photon[i].momentum());

       sortByE(pmupairs);
       sortByE(pzprime);
       sortByE(pphoton);

       int nz = pzprime.size();
       int np = pphoton.size();

       for (int i=0; i < 10; ++i)
         if (int(electrons.size()+muons.size()) >= i)
           _h["nlepton"]->fill(i);
       for (int i=0; i < 10; ++i)
         if (int(zprime.size()) >= i)
           _h["nzprime"]->fill(i);
       for (int i=0; i < 10; ++i)
         if (int(photon.size()) >= i)
           _h["nphoton"]->fill(i);

       if (electrons.size()>0) _h["elepton"]->fill(electrons.front().momentum().E());
       if (nz>0) _h["ezprime"]->fill(pzprime.front().E());
       if (np>0) _h["ephoton"]->fill(pphoton.front().E());

       if (electrons.size()>0) _h["ptlepton"]->fill(electrons.front().momentum().pT());
       if (nz>0) _h["ptzprime"]->fill(log10(pzprime.front().pT()));
       if (np>0) _h["ptphoton"]->fill(log10(pphoton.front().pT()));

       double emin=0.1;
       if (electrons.size()>0 && np>0 && pphoton.front().E()>emin)
         _h["elepton_photonevt"]->fill(electrons.front().momentum().E());
       if (electrons.size()>0 && np>0 && pphoton.front().E()>emin)
         _h["ptlepton_photonevt"]->fill(electrons.front().momentum().pT());

       if (electrons.size()>0 && nz>0 && pzprime.front().E()>0.)
         _h["elepton_zprimeevt"]->fill(electrons.front().momentum().E());
       if (electrons.size()>0 && nz>0 && pzprime.front().E()>0.)
         _h["ptlepton_zprimeevt"]->fill(electrons.front().momentum().pT());

    }

    /// Normalise histograms etc., after the run
    void finalize() {
      double scalefactor( crossSection() / sumOfWeights() );

      for ( auto h : _h) scale (h.second,scalefactor);

    }

    map<string, Histo1DPtr> _h;
      
  };

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MLL);

}
