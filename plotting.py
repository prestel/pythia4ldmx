import os, sys, subprocess, math, pythia8
from IPython import display
import matplotlib.pyplot as plt
from ipywidgets import interact, Layout
import ipywidgets as wgs

class PDF(object):
    def __init__(self, pdf, size=(200,200)):
        self.pdf = pdf
        self.size = size
    def _repr_html_(self):
        return '<iframe src={0} width={1[0]} height={1[1]}></iframe>'.format(self.pdf, self.size)
    def _repr_latex_(self):
        return r'\includegraphics[width=1.0\textwidth]{{{0}}}'.format(self.pdf)

class RUNRIVET(object):
    def __init__(self):
        # Defaults.
        self.anas   = []
        self.file   = ''
        # Setup the options for the menu.
        ana_options = ['ALEPH_1996_S3486095 (studies of QCD at 91.2 GeV)', \
                       'ALEPH_2004_S5765862 (jet rates and event shapes at 91.2 - 206 GeV)', \
                       'ALICE_2015_I1357424 (pT spectra of pions, kaons and protons at 7 TeV)', \
                       'ATLAS_2011_S8924791 (jet shapes at 7 TeV)', \
                       'ATLAS_2011_S8971293 (dijet azimuthal decorrelations at 7 TeV)', \
                       'ATLAS_2012_I1119557 (jet shapes and jet masses)', \
                       'MC_DIS_Check (simple analysis to illustrate DISKinematics)', \
                       'H1_1994_S2919893 (energy flow and charged particle spectra in DIS)']
        style       = {'description_width': 'initial'}
        layout      = Layout(width='800px')
        message     = 'You can now select from a range of analyses and/or add analyses:'
        # Setup the menu and checkbox.
        w_anas      = wgs.SelectMultiple(options=ana_options, value=[ana_options[0]], \
                                         rows=len(ana_options), description='Analyses', \
                                         style=style, layout=layout, disabled=False)
        w_addanas   = wgs.Text(value='', placeholder='Type something', \
                               description='Comma-separated list of analyses', \
                               style=style, layout=layout, disabled=False)
        def apply_ana_infos(anas, addanas):
            self.anas   = []
            for ana in anas:
                self.anas.append(ana[:ana.find('(')-1])
            if len(addanas) > 2:
                for ana in addanas.split(','):
                    self.anas.append(ana)
        print (message)
        interact(apply_ana_infos, anas = w_anas, addanas = w_addanas)
    def start_rivet(self, hepmcfilename, nEvent=-1):
        self.file = hepmcfilename
        ## Make fifo pipe -- does not seem to work for Rivet 3
        #if os.path.exists(self.file): os.remove(self.file)
        #os.mkfifo(self.file)
        startrivet  = 'rivet ' 
        for ana in self.anas:
            startrivet += '-a '+ana+' '
        if nEvent > 0:
            startrivet += '-n '+str(nEvent)+' -H '+self.file+'.yoda '+self.file
        else:
            startrivet += '-H '+self.file+'.yoda '+self.file
        subprocess.call(["bash","-c",startrivet])
    def produce_plots(self):
        ## Remove fifo pipe.
        #os.remove(self.file)
        # Produce rivet plots.
        plotrivet = 'rivet-mkhtml --mc-errs --booklet '+self.file+'.yoda'
        subprocess.call(["bash","-c",plotrivet])

class MULTHIST(object):
    def __init__(self):
        # Defaults.
        dSigma     = '$1/\\sigma\,\,$d$\,\\sigma/$d$\,$'
        self.nHist = 0
        self.part_id, x_labels = [], []
        self.nBin, self.xMin, self.xMax = [], [], []
        self.plot_info, self.histos = [], []
        self.fig, self.axes = None, []
        # Setup the options for the menu.
        mult_ids   = ['p(bar)', 'pi+-', 'K+-']
        id_convert = {'p(bar)': [2212, 'p/\\bar p'], 'pi+-': [211, '\\pi^\\pm'], \
                      'K+-': [321, 'K^\\pm']}
        style      = {'description_width': 'initial'}
        layout     = Layout(width='150px')
        message    = 'You can now select for which particles you want to ' \
                     + 'histogram the multiplicty and whether a logarithmic '\
                     + 'y-axis should be used:'
        # Setup the menu and checkbox.
        w_multis   = wgs.SelectMultiple(options=mult_ids, value=['p(bar)', 'pi+-'], \
                                        rows=len(mult_ids), description='Particles', \
                                        style=style, layout=layout, disabled=False)
        w_logyaxis = wgs.Checkbox(value=True, description='logarithmic y-axis', \
                                  style=style, layout=layout, disabled=False)
        def apply_hist_infos(multis, logyaxis):
            self.nHist = len(multis)
            # List of particle ids and names for axes labels.
            self.part_id, x_labels = [], []
            for i in range(self.nHist):
                self.part_id.append(id_convert[multis[i]][0])
                x_labels.append('$n_{'+id_convert[multis[i]][1]+'}$')
            # Histogram binning.
            self.nBin, self.xMin, self.xMax = \
              [100]*self.nHist, [-0.5]*self.nHist, [99.5]*self.nHist
            for i in range(self.nHist):
                if self.part_id[i] == 2212 or self.part_id[i] == 321:
                    self.nBin[i] = 50
                    self.xMax[i] = 49.5
                elif self.part_id[i] == 211 or self.part_id[i] == 111:
                    self.xMax[i] = 299.5
            # plot_info = list of [title, xlabel, ylabel, color, log y-axis]
            self.plot_info, self.histos = [], []
            for i in range(self.nHist):
                self.plot_info.append([x_labels[i]+' Distribution', x_labels[i], \
                                       dSigma+x_labels[i], 'black', logyaxis])
                self.histos.append(pythia8.Hist(multis[i]+' multiplicity', \
                                                self.nBin[i], self.xMin[i], self.xMax[i]))
        print (message)
        interact(apply_hist_infos, multis = w_multis, logyaxis = w_logyaxis)
    def analyze_event(self, event):
        # Find number of interesting final particles and fill histograms.
        finalCount = {}
        for prt in event:
            if prt.isFinal() and prt.idAbs() in self.part_id:
                if prt.idAbs() in finalCount.keys():
                    finalCount[prt.idAbs()] += 1
                else:
                    finalCount[prt.idAbs()] = 0
        for i in range(self.nHist):
            if self.part_id[i] in finalCount.keys():
                self.histos[i].fill(finalCount[self.part_id[i]])
    def add_plot(self, axis, hist, col, nBin, xMin, xMax):
        dx = (xMax-xMin)/(nBin+0.0)
        x, y, yErr = [], [], []
        sumNow = 0.0
        for i in range(1,nBin+1):
            xNow, yNow = xMin + 0.5*dx + ((i-1)*dx), hist.getBinContent(i)
            if yNow >= 0.0: yErrNow = math.sqrt(yNow)
            else: yErrNow = math.sqrt(-yNow)
            sumNow += dx*yNow
            x.append(xNow)
            y.append(yNow)
            yErr.append(yErrNow)
        if sumNow != 0.0:
            for i in range(len(y)):
                y[i] /= sumNow
                yErr[i] /= sumNow
        axis.errorbar(x, y, yerr=yErr, linestyle='-', linewidth=1, \
                      color=col, capsize=0, drawstyle='steps-mid')
        return axis
    def setup_plots(self):
        # Setup figure, first time only.
        if self.fig == None:
            nrow, ncol = (1+self.nHist)//2, 2
            if self.nHist == 1: nrow, ncol = 1, 1
            self.fig, axs = plt.subplots(figsize=(5*ncol, 4*nrow), \
                                         nrows=nrow, ncols=ncol)
            self.axes = []
            if self.nHist == 1:
                self.axes.append(axs)
            else:
                for axis in axs.flatten(): self.axes.append(axis)
        plt.tight_layout(pad=1.0, w_pad=4.0, h_pad=2.0)
        for i in range(len(self.axes)):
            self.axes[i].clear()
            if i >= self.nHist:
                self.axes[i].axis('off')
                continue
            self.axes[i].set_title(self.plot_info[i][0], fontsize=16, \
                                   color='black')
            self.axes[i].set_xlabel(self.plot_info[i][1], fontsize=12, \
                                    color='black')
            self.axes[i].set_ylabel(self.plot_info[i][2], fontsize=12, \
                                    color='black')
            self.axes[i] = self.add_plot(self.axes[i], self.histos[i], \
                                         self.plot_info[i][3], self.nBin[i], \
                                         self.xMin[i], self.xMax[i])
            self.axes[i].set_xlim(xmin=self.xMin[i],xmax=self.xMax[i])
            if (self.plot_info[i][4]):
                for j in range(1,self.nBin[i]+1):
                    if self.histos[i].getBinContent(j) > 0.0:
                        self.axes[i].set_yscale('log', nonposy='clip')
                        break
    def print_plots(self, filename):
        self.setup_plots()
        plt.gcf().savefig(filename)
        display.clear_output(wait=True)
        display.display(display.Image(filename))
